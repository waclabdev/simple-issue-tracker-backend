const mongoose = require('mongoose');

var issueSchema = mongoose.Schema({
	content:  String,
	title: String,
	date: String,
	status: String
});

const Issue = module.exports = mongoose.model('Issue', issueSchema);

// Get Issues
module.exports.getIssues = (callback, limit) => {
	Issue.find(callback).limit(limit);
};

// Get single Issue
module.exports.getIssueById = (id, callback) => {
	Issue.findById(id, callback);
};

// Add Issue
module.exports.addIssue = (issue, callback) => {
	Issue.create(issue, callback);
};

// Update Issue
module.exports.updateIssue = (id, issue, options, callback) => {
	var query = {_id: id};
	var update = {
		title: issue.title,
		content:  issue.content,
		date: issue.date,
		status: issue.status
	};
	Issue.findOneAndUpdate(query, update, options, callback);
};

// Delete Book
module.exports.removeIssue = (id, callback) => {
	var query = {_id: id};
	Issue.remove(query, callback);
};
