const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes/routes');
const config = require('./config/config');

app.use(express.static(__dirname+'/client'));
app.use(bodyParser.json());

// Connect to Mongoose
mongoose.connect(config.db, { useMongoClient: true });
mongoose.Promise = global.Promise;

app.use(cors());
app.use('/', routes);

app.listen(config.port);
console.log(`Running on port ${config.port}...`);
