var express = require('express');
const router = express.Router();
Issue = require('../models/issue');

router.get('/api/issues', (req, res) => {
	Issue.getIssues((err, issues) => {
	if(err){
		throw err;
	}
		res.json(issues);
	});
});

router.get('/api/issues/:_id', (req, res) => {
	Issue.getIssueById(req.params._id, (err, issue) => {
	if(err){
		throw err;
	}
		res.json(issue);
	});
});

router.post('/api/issues', (req, res) => {
	var issue = req.body;
	Issue.addIssue(issue, (err, issue) => {
		if(err){
			throw err;
		}
		res.json(issue);
	});
});

router.put('/api/issues/:_id', (req, res) => {
	var id = req.params._id;
	var issue = req.body;
	Issue.updateIssue(id, issue, {}, (err, issue) => {
		if(err){
			throw err;
		}
		res.json(issue);
	});
});

router.delete('/api/issues/:_id', (req, res) => {
	var id = req.params._id;
	Issue.removeIssue(id, (err, issue) => {
		if(err){
			throw err;
		}
		res.json(issue);
	});
});

module.exports = router;